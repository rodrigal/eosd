FROM fedora:23

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

RUN echo $'\n\
[eos-citrine] \n\
name=EOS 4.0 Version \n\
baseurl=https://dss-ci-repo.web.cern.ch/dss-ci-repo/eos/citrine/tag/fc-23/x86_64/ \n\
gpgcheck=0 \n'\
>> /etc/yum.repos.d/eos.repo

RUN dnf install -y \
	eos-fuse \
	eos-fuse-core \
	eos-fuse-sysv \
	initscripts \
	procps-ng

COPY eos /etc/sysconfig/eos
COPY eos.user /etc/sysconfig/eos.user

ADD launch.sh /launch.sh

ENTRYPOINT ["/launch.sh"]
