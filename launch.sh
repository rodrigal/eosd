#!/bin/bash
INSTANCE=$1

# check the required config is available
[ -f /etc/sysconfig/eos ] && . /etc/sysconfig/eos
if [ "$INSTANCE" != "main" ]; then
    if [ -f /etc/sysconfig/eos.$INSTANCE ]; then
        . /etc/sysconfig/eos.$INSTANCE
    else
        echo -n "Skipping fuse mount for instance $INSTANCE - no /etc/sysconfig/eos.$INSTANCE configuration file"
	exit -1
    fi
fi

# export env vars
export EOS_FUSE_DEBUG=${EOS_FUSE_DEBUG-0}
export EOS_FUSE_LOWLEVEL_DEBUG=${EOS_FUSE_LOWLEVEL_DEBUG-0}
export EOS_FUSE_NOACCESS=${EOS_FUSE_NOACCESS-1}
export EOS_FUSE_SYNC=${EOS_FUSE_SYNC-0}
export EOS_FUSE_KERNELCACHE=${EOS_FUSE_KERNELCACHE-1}
export EOS_FUSE_DIRECTIO=${EOS_FUSE_DIRECTIO-0}
export EOS_FUSE_CACHE=${EOS_FUSE_CACHE-1}
export EOS_FUSE_CACHE_SIZE=${EOS_FUSE_CACHE_SIZE-67108864}
export EOS_FUSE_CACHE_PAGE_SIZE=${EOS_FUSE_CACHE_PAGE_SIZE-262144}
export EOS_FUSE_BIGWRITES=${EOS_FUSE_BIGWRITES-1}    
export EOS_FUSE_EXEC=${EOS_FUSE_EXEC-0}
export EOS_FUSE_NO_MT=${EOS_FUSE_NO_MT-0}
export EOS_FUSE_USER_KRB5CC=${EOS_FUSE_USER_KRB5CC-0}
export EOS_FUSE_USER_UNSAFEKRB5=${EOS_FUSE_USER_UNSAFEKRB5-0}
export EOS_FUSE_USER_GSIPROXY=${EOS_FUSE_USER_GSIPROXY-0}
export EOS_FUSE_USER_KRB5FIRST=${EOS_FUSE_USER_KRB5FIRST-0}
export EOS_FUSE_FALLBACKTONOBODY=${EOS_FUSE_FALLBACKTONOBODY-0}
export EOS_FUSE_PIDMAP=${EOS_FUSE_PIDMAP-0}
export EOS_FUSE_RMLVL_PROTECT=${EOS_FUSE_RMLVL_PROTECT-1}
export EOS_FUSE_RDAHEAD=${EOS_FUSE_RDAHEAD-0}
export EOS_FUSE_RDAHEAD_WINDOW=${EOS_FUSE_RDAHEAD_WINDOW-131072}
export EOS_FUSE_LAZYOPENRO=${EOS_FUSE_LAZYOPENRO-0}
export EOS_FUSE_LAZYOPENRW=${EOS_FUSE_LAZYOPENRW-1}
export EOS_FUSE_ASYNC_OPEN=${EOS_FUSE_ASYNC_OPEN-0}
export EOS_FUSE_SHOW_SPECIAL_FILES=${EOS_FUSE_SHOW_SPECIAL_FILES-0}
export EOS_FUSE_SHOW_EOS_ATTRIBUTES=${EOS_FUSE_SHOW_EOS_ATTRIBUTES-0}
export EOS_FUSE_INLINE_REPAIR=${EOS_FUSE_INLINE_REPAIR-1}
export EOS_FUSE_MAX_INLINE_REPAIR_SIZE=${EOS_FUSE_MAX_INLINE_REPAIR_SIZE-268435456}
export EOS_FUSE_LOG_PREFIX=${EOS_FUSE_LOG_PREFIX-$INSTANCE}
export EOS_FUSE_ENTRY_CACHE_TIME=${EOS_FUSE_ENTRY_CACHE_TIME-10}
export EOS_FUSE_ATTR_CACHE_TIME=${EOS_FUSE_ATTR_CACHE_TIME-10}
export EOS_FUSE_NEG_ENTRY_CACHE_TIME=${EOS_FUSE_NEG_ENTRY_CACHE_TIME-30}
export EOS_FUSE_CREATOR_CAP_LIFETIME=${EOS_FUSE_CREATOR_CAP_LIFETIME-30}
export EOS_FUSE_FILE_WB_CACHE_SIZE=${EOS_FUSE_FILE_WB_CACHE_SIZE-67108864}
export EOS_FUSE_MOUNTDIR=${EOS_FUSE_MOUNTDIR-/eos/}
export EOS_FUSE_REMOTEDIR=${EOS_FUSE_REMOTEDIR-${EOS_FUSE_MOUNTDIR}}
export EOS_FUSE_MAX_WB_INMEMORY_SIZE=${EOS_FUSE_MAX_WB_INMEMORY_SIZE-536870912}
export XRD_RUNFORKHANDLER=1
[[ "x$EOS_FUSE_STREAMERRORWINDOW" != "x" ]] && export XRD_STREAMERRORWINDOW=$EOS_FUSE_STREAMERRORWINDOW

export FUSE_OPT=${FUSE_OPT-"${FUSE_OPT_SAVE}"}

if [ "x$EOS_FUSE_BIGWRITES" = "x1" ]; then
    FUSE_OPT="big_writes,"${FUSE_OPT-""}
else
    test -n "${FUSE_OPT}" && FUSE_OPT=${FUSE_OPT}","
fi

# try to unmount to cleanup 
echo unmounting and cleaning mountpoint ${EOS_FUSE_MOUNTDIR} for dead but subsys locked instance $INSTANCE
umount -f ${EOS_FUSE_MOUNTDIR} > /dev/null 2>&1

# launch eosd
echo launching eosd $INSTANCE
unset KRB5CCNAME
unset X509_USER_CERT
unset X509_USER_KEY
ulimit -S -c ${DAEMON_COREFILE_LIMIT:-0}
ulimit -n 65000
test -c /dev/fuse || modprobe fuse
echo starting eosd
/usr/bin/eosd ${EOS_FUSE_MOUNTDIR} -o${FUSE_OPT-}max_readahead=131072,max_write=4194304,fsname=eos$INSTANCE,allow_other,url=root://${EOS_FUSE_MGM_ALIAS-localhost}/${EOS_FUSE_REMOTEDIR}

echo "EOS_FUSE_DEBUG                   : ${EOS_FUSE_DEBUG}"
echo "EOS_FUSE_LOWLEVEL_DEBUG          : ${EOS_FUSE_DEBUG}"
echo "EOS_FUSE_NOACCESS                : ${EOS_FUSE_NOACCESS}"
echo "EOS_FUSE_SYNC                    : ${EOS_FUSE_SYNC}"
echo "EOS_FUSE_KERNELCACHE             : ${EOS_FUSE_KERNELCACHE}"
echo "EOS_FUSE_DIRECTIO                : ${EOS_FUSE_DIRECTIO}"
echo "EOS_FUSE_CACHE                   : ${EOS_FUSE_CACHE}"
echo "EOS_FUSE_CACHE_SIZE              : ${EOS_FUSE_CACHE_SIZE}"
echo "EOS_FUSE_CACHE_PAGE_SIZE         : ${EOS_FUSE_CACHE_PAGE_SIZE}"
echo "EOS_FUSE_BIGWRITES               : ${EOS_FUSE_BIGWRITES}"
echo "EOS_FUSE_EXEC                    : ${EOS_FUSE_EXEC}"
echo "EOS_FUSE_NO_MT                   : ${EOS_FUSE_NO_MT}"
echo "EOS_FUSE_USER_KRB5CC             : ${EOS_FUSE_USER_KRB5CC}"
echo "EOS_FUSE_USER_GSIPROXY           : ${EOS_FUSE_USER_GSIPROXY}"
echo "EOS_FUSE_USER_KRB5FIRST          : ${EOS_FUSE_USER_KRB5FIRST}"
echo "EOS_FUSE_PIDMAP                  : ${EOS_FUSE_PIDMAP}"
echo "EOS_FUSE_RMLVL_PROTECT           : ${EOS_FUSE_RMLVL_PROTECT}"
echo "EOS_FUSE_RDAHEAD                 : ${EOS_FUSE_RDAHEAD}"
echo "EOS_FUSE_RDAHEAD_WINDOW          : ${EOS_FUSE_RDAHEAD_WINDOW}"
echo "EOS_FUSE_LAZYOPENRO              : ${EOS_FUSE_LAZYOPENRO}"
echo "EOS_FUSE_LAZYOPENRW              : ${EOS_FUSE_LAZYOPENRW}"
echo "EOS_FUSE_ASYNC_OPEN              : ${EOS_FUSE_ASYNC_OPEN}"
echo "EOS_FUSE_SHOW_SPECIAL_FILES      : ${EOS_FUSE_SHOW_SPECIAL_FILES}"
echo "EOS_FUSE_SHOW_EOS_ATTRIBUTES     : ${EOS_FUSE_SHOW_EOS_ATTRIBUTES}"
echo "EOS_FUSE_INLINE_REPAIR           : ${EOS_FUSE_INLINE_REPAIR}"
echo "EOS_FUSE_MAX_INLINE_REPAIR_SIZE  : ${EOS_FUSE_MAX_INLINE_REPAIR_SIZE}"
echo "EOS_FUSE_ATTR_CACHE_TIME         : ${EOS_FUSE_ATTR_CACHE_TIME}"
echo "EOS_FUSE_ENTRY_CACHE_TIME        : ${EOS_FUSE_ENTRY_CACHE_TIME}"
echo "EOS_FUSE_NEG_ENTRY_CACHE_TIME    : ${EOS_FUSE_NEG_ENTRY_CACHE_TIME}"
echo "EOS_FUSE_CREATOR_CAP_LIFETIME    : ${EOS_FUSE_CREATOR_CAP_LIFETIME}"
echo "EOS_FUSE_FILE_WB_CACHE_SIZE      : ${EOS_FUSE_FILE_WB_CACHE_SIZE}"
echo "EOS_FUSE_MAX_WB_INMEMORY_SIZE    : ${EOS_FUSE_MAX_WB_INMEMORY_SIZE}"

echo "EOS_FUSE_LOG_PREFIX              : ${EOS_FUSE_LOG_PREFIX}"
echo "EOS_FUSE_MOUNTDIR                : ${EOS_FUSE_MOUNTDIR}"
echo "EOS_FUSE_REMOTEDIR               : ${EOS_FUSE_REMOTEDIR}"

[[ "x$XRD_STREAMERRORWINDOW" != "x" ]] && echo "XRD_STREAMERRORWINDOW        : ${XRD_STREAMERRORWINDOW}"

for i in 1 10; do tail -f /var/log/eos/fuse/fuse.$INSTANCE.log; sleep 1; done
